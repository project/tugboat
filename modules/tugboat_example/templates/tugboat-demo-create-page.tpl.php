<?php
/**
 * Template for the create demo page.
 */
?>

<div class="tugboat-demo-create-page">
  <p class="tugboat-demo-create-intro">
    Get started by clicking the button below! Creating a new site may take a few minutes.
  </p>

  <?php print render($form); ?>
</div>
